#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <SDL.h>
#include "fontlib.h"
#include "conlib.h"
#include "renlib.h"
#include "guilib.h"

/****************************************************************************
 ** widcls_button - Basic pushbutton widget.                               **
 ****************************************************************************/

#define BN_RELEASED   0
#define BN_DEPRESSED  1
#define BN_ROLLOVER   2
#define BN_LATCH      4
#define BN_STATEMASK  (BN_DEPRESSED | BN_ROLLOVER)

typedef struct wid_button_s
{
	SDL_Surface  *images[5];
	char         label[64];
	Uint8        state;
} wid_button_t;

static int wid_button_create(HWIDGET widget, WID_CreateEvent *event)
{
	wid_button_t *ldata;

	ldata = malloc(sizeof(wid_button_t));

	if (!ldata)
		Error("wid_button_create: Out of memory\n");

	SetWidgetData(widget, ldata);
	memset(ldata, 0, sizeof(wid_button_t));

	return 1;
}

static void wid_button_destroy(wid_button_t *ldata)
{
	if (ldata);
		free(ldata);
}

static int wid_button_rollover(WID_RolloverEvent *event, wid_button_t *ldata)
{
	if (event->leaving)
	{
		/* disable rollover/depressed states, but leave latched */
		ldata->state &= ~(BN_DEPRESSED | BN_ROLLOVER);
	}
	else
	{
		/* enable depressed state if latched, or rollover state if not latched */
		if (ldata->state & BN_LATCH)
			ldata->state |= BN_DEPRESSED;

		ldata->state |= BN_ROLLOVER;
	}

	return 1;
}

static int wid_button_mousebuttondown(HWIDGET widget, SDL_MouseButtonEvent *event, wid_button_t *ldata)
{
	if (event->button != SDL_BUTTON_LEFT)
		return 0;

	SetCaptureWidget(widget);
	
	ldata->state |= BN_DEPRESSED | BN_LATCH;

	return 1;
}

static int wid_button_mousebuttonup(HWIDGET widget, SDL_MouseButtonEvent *event, wid_button_t *ldata)
{
	Uint32 flags;

	if (event->button != SDL_BUTTON_LEFT)
		return 0;

	SetCaptureWidget(0);

	flags = GetWidgetFlags(widget);

	if (ldata->state & BN_DEPRESSED && flags & WIDGET_ACTIVE)
		DispatchWidgetCommand(widget, GetWidgetID(widget));

	ldata->state &= ~(BN_DEPRESSED | BN_LATCH);

	return 1;
}

static int wid_button_eventcb(HWIDGET widget, SDL_Event *event, void *data)
{
	switch (event->type)
	{
	case WID_CREATE:
		return wid_button_create(widget, (WID_CreateEvent *)event);

	case WID_DESTROY:
		wid_button_destroy((wid_button_t *)data);
		return 1;

	case WID_ROLLOVER:
		return wid_button_rollover((WID_RolloverEvent *)event, (wid_button_t *)data);
		
	case SDL_MOUSEBUTTONDOWN:
		return wid_button_mousebuttondown(widget, (SDL_MouseButtonEvent *)event, (wid_button_t *)data);

	case SDL_MOUSEBUTTONUP:
		return wid_button_mousebuttonup(widget, (SDL_MouseButtonEvent *)event, (wid_button_t *)data);
	}

	return DefaultEventHandler(widget, event, data);
}

static void wid_button_drawcb(HWIDGET widget, const rect_t *rect, void *data)
{
	wid_button_t  *ldata;
	SDL_Surface   *image;
	SDL_Rect      srcrect, dstrect;
	Uint32        flags;

	ldata = (wid_button_t *)data;
	flags = GetWidgetFlags(widget);

	if (flags & WIDGET_ACTIVE)
		image = ldata->images[ldata->state & BN_STATEMASK];
	else
		image = ldata->images[4];

	if (image)
	{
		/* left edge */
		srcrect.x = 0;
		srcrect.y = 0;
		srcrect.w = 4;
		srcrect.h = rect->h;

		dstrect.x = rect->x;
		dstrect.y = rect->y;
		dstrect.w = 4;
		dstrect.h = rect->h;

		SDL_BlitSurface(image, &srcrect, framebuf, &dstrect);

		/* middle */
		srcrect.x = 4;
		srcrect.y = 0;
		srcrect.w = rect->w - 8;
		srcrect.h = rect->h;

		dstrect.x = rect->x + 4;
		dstrect.y = rect->y;
		dstrect.w = srcrect.w;
		dstrect.h = rect->h;

		SDL_BlitSurface(image, &srcrect, framebuf, &dstrect);

		/* right edge */
		srcrect.x = 300;
		srcrect.y = 0;
		srcrect.w = 4;
		srcrect.h = rect->h;

		dstrect.x = rect->x + rect->w - 4;
		dstrect.y = rect->y;
		dstrect.w = 4;
		dstrect.h = rect->h;

		SDL_BlitSurface(image, &srcrect, framebuf, &dstrect);
	}

	DrawText(deffont, framebuf, JUSTIFY_CENTER, rect->x + rect->w/2, rect->y + rect->h/2 - deffont->h/2, ldata->label);
}

widcls_t widcls_button =
{
	wid_button_eventcb,
	wid_button_drawcb
};

void SetButtonImages(HWIDGET button, SDL_Surface *released, SDL_Surface *depressed, SDL_Surface *rollover, SDL_Surface *inactive)
{
	wid_button_t *ldata;

	ldata = (wid_button_t *)GetWidgetData(button);
	ldata->images[0] = released;
	ldata->images[1] = depressed;
	ldata->images[2] = rollover;
	ldata->images[3] = depressed;		/* depressed+rollover state */
	ldata->images[4] = inactive;
}

void SetButtonLabel(HWIDGET button, const char *text)
{
	wid_button_t *ldata;

	ldata = (wid_button_t *)GetWidgetData(button);
	strncpy(ldata->label, text, sizeof(ldata->label)/sizeof(char)-1);
}

/****************************************************************************
 ** widcls_picture - Static image blitting widget.                         **
 ****************************************************************************/

void wid_picture_drawcb(HWIDGET widget, const rect_t *rect, void *data)
{
	SDL_Surface  *ldata;
	SDL_Rect     dr;

	ldata = (SDL_Surface *)data;

	if (ldata)
	{
		dr.x = rect->x;
		dr.y = rect->y;
		dr.w = ldata->w;
		dr.h = ldata->h;

		SDL_BlitSurface(ldata, 0, framebuf, &dr);
	}
}

widcls_t widcls_picture =
{
	DefaultEventHandler,
	wid_picture_drawcb
};
