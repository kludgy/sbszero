#include <SDL.h>
#include "conlib.h"

SDL_Surface *framebuf = 0;   /* available to other modules */

static struct
{
	int     w, h;            /* width/height of view in pixels */
	int     depth;           /* bit depth                      */
	Uint32  flags;           /* SDL surface flags              */
} vi;

/*
 * r_Init()
 *
 * Initialize the framebuffer surface.
 */
void r_Init(void)
{
	if (framebuf)
		Warning("r_Init: Framebuffer already initialized\n");

	/* creation parameters */
	vi.w     = 640;
	vi.h     = 480;
	vi.depth = 0;
	vi.flags = SDL_HWSURFACE;

	/* create the view */
	framebuf = SDL_SetVideoMode(vi.w, vi.h, vi.depth, vi.flags);
	
	if (!framebuf) 
		Error("r_Init: Couldn't set video mode: %s\n", SDL_GetError());
}
