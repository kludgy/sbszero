#include <SDL.h>
#include "renlib.h"
#include "conlib.h"
#include "sprite.h"

static SDL_Surface  *scenebufs[MAXSPRITES];		/* background recovery bitmaps */
sprite_t            sprites[MAXSPRITES];

static void FreeSprites(void)
{
	int i;

	/* free remaining scenebufs */
	for (i = 0; i < MAXSPRITES; i++)
		if (scenebufs[i]) SDL_FreeSurface(scenebufs[i]);
}

void InitSprites(void)
{
	atexit(FreeSprites);
	memset(scenebufs, 0, sizeof(scenebufs));
	memset(sprites, 0, sizeof(sprites));
}

void SetSpriteDims(int spritenum, int w, int h)
{
	SDL_Surface      **buf;
	SDL_PixelFormat  *vfmt;

	buf = scenebufs + spritenum;

	if (*buf)
		SDL_FreeSurface(*buf);

	if (w && h)
	{
		/* resize */

		vfmt = framebuf->format;

		*buf = SDL_CreateRGBSurface(
			SDL_HWSURFACE, w, h, 
			vfmt->BitsPerPixel,
			vfmt->Rmask,
			vfmt->Gmask,
			vfmt->Bmask,
			vfmt->Amask
		);

		if (!buf)
			Error("SetSpriteDims: %s\n", SDL_GetError());
	}
	else
		*buf = NULL;		/* w or h zero to delete the scenebuf */
}

void DrawSprites(void)
{
	sprite_t     *spr;
	SDL_Surface  **buf;
	SDL_Rect     srcrect, dstrect;
	int          i;

	spr = sprites;
	buf = scenebufs;

	for (i = 0; i < MAXSPRITES; i++, spr++, buf++)
	{
		if (!spr->image || !*buf)
			continue;

		/* fill scenebufs */

		srcrect.x = spr->x;
		srcrect.y = spr->y;
		srcrect.w = (*buf)->w;
		srcrect.h = (*buf)->h;

		SDL_BlitSurface(framebuf, &srcrect, *buf, NULL);

		/* draw image */

		if (spr->rect)
		{
			/* subimage */

			srcrect = *spr->rect;
			dstrect.x = spr->x;
			dstrect.y = spr->y;
			dstrect.w = srcrect.w;
			dstrect.h = srcrect.h;

			SDL_BlitSurface(spr->image, &srcrect, framebuf, &dstrect);
		}
		else
		{
			/* whole image */

			dstrect.x = spr->x;
			dstrect.y = spr->y;
			dstrect.w = spr->image->w;
			dstrect.h = spr->image->h;

			SDL_BlitSurface(spr->image, NULL, framebuf, &dstrect);
		}
	}
}

void EraseSprites(void)
{
	sprite_t     *spr;
	SDL_Surface  **buf;
	SDL_Rect     dstrect;
	int          i;

	spr = sprites;
	buf = scenebufs;

	for (i = 0; i < MAXSPRITES; i++, spr++, buf++)
	{
		if (!spr->image || !*buf)
			continue;

		/* replace framebuf with contents from scenebufs */

		dstrect.x = spr->x;
		dstrect.y = spr->y;
		dstrect.w = (*buf)->w;
		dstrect.h = (*buf)->h;

		SDL_BlitSurface(*buf, NULL, framebuf, &dstrect);
	}
}
