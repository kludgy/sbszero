typedef enum screen_e
{
	SCREEN_FRONTEND = 0,
	SCREEN_CLIENT,
	NUMSCREENS
} screen_t;

extern void CreateScreens(void);
extern void ShowScreen(screen_t);
