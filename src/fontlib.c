#include <stdlib.h>
#include <string.h>
#include <SDL.h>
#include "fontlib.h"
#include "conlib.h"

static Uint32 GetPixel(SDL_Surface *Surface, Sint32 X, Sint32 Y)
{
	Uint8   *bits;
	Uint32  Bpp;

	Bpp = Surface->format->BytesPerPixel;
	bits = ((Uint8 *)Surface->pixels)+Y*Surface->pitch+X*Bpp;

	/* get the pixel colour */
	switch(Bpp) 
	{
		case 1:
			return *((Uint8 *)Surface->pixels + Y * Surface->pitch + X);

		case 2:
			return *((Uint16 *)Surface->pixels + Y * Surface->pitch/2 + X);

		case 3: 
		{
			/* Format/endian independent */
			Uint8 r, g, b;

			r = *((bits)+Surface->format->Rshift/8);
			g = *((bits)+Surface->format->Gshift/8);
			b = *((bits)+Surface->format->Bshift/8);

			return SDL_MapRGB(Surface->format, r, g, b);
		}
		
		case 4:
			return *((Uint32 *)Surface->pixels + Y * Surface->pitch/4 + X);
	}

	return -1;
}

#define TAGPEL(src, x) (GetPixel(src, x, 0) == SDL_MapRGB(src->format, 255, 0, 255))

font_t *CreateFont(SDL_Surface *src)
{
	int     i, j, w;
	font_t  *font;

	if (!src)
		Error("CreateFont: Null source\n");

	font = malloc(sizeof(font_t));

	if (!font)
		Error("CreateFont: Out of memory\n");

	memset(font, 0, sizeof(font_t));
	font->src = src;
	font->h = src->h;

	/* calc character bitmap offsets */
	j = 0;
	i = 0;

	while (1)
	{
		while (i < src->w && TAGPEL(src, i))	/* skip initial tag */
			i++;

		if (i >= src->w)
			break;								/* all done */

		if (j >= MAXFONTCHARS)
			Error("InitFont: Too many characters in source bitmap\n");

		font->x[j] = i;
		w = 0;

		while (i < src->w && !TAGPEL(src, i))	/* get the width */
		{
			i++;
			w++;
		}

		font->w[j] = w;
		j++;
	}

	if (src->w && src->h)
	    SDL_SetColorKey(src, SDL_SRCCOLORKEY, GetPixel(src, 0, src->h - 1));

	return font;
}

void FreeFont(font_t *font)
{
	free(font);
}

int TextWidth(font_t *font, const char *text)
{
	int         x;
	const char  *s;

    for (x = 0, s = text; *s; s++)
		x += font->w[*s - 32];

    return x;
}

void DrawText(font_t *font, SDL_Surface *dest, int justify, int x, int y, const char *text)
{
	int         c, w;
	const char  *s;
	SDL_Rect    sr, dr;

	switch (justify)
	{
		case JUSTIFY_CENTER: x -= TextWidth(font, text) / 2; break;
		case JUSTIFY_RIGHT: x -= TextWidth(font, text); break;
		default:; /* assume JUSTIFY_LEFT and do nothing */
	}

	for (s = text; *s; s++)
	{
		if (*s == ' ')			/* don't perform blits for whitespace */
		{
            x += font->w[0];
		}
		else
		{
			c = (int)*s - 32;
			w = font->w[c];

            sr.x = font->x[c];
            sr.y = 1;
            sr.w = w;
            sr.h = font->h - 1;

    	    dr.x = x;
			dr.y = y;
			dr.w = sr.w;
			dr.h = sr.h;

			SDL_BlitSurface(font->src, &sr, dest, &dr);

            x += w;
		}
	}
}
