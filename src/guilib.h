typedef struct rect_s
{
	int x, y;
	int w, h;
} rect_t;

typedef struct widget_s *HWIDGET;

/* widget event types */
#define WID_CREATE    (SDL_USEREVENT+0)		/* WID_CreateEvent	 */
#define WID_DESTROY   (SDL_USEREVENT+1)		/* WID_DestroyEvent	 */
#define WID_COMMAND   (SDL_USEREVENT+2)		/* WID_CommandEvent	 */
#define WID_SHAPE     (SDL_USEREVENT+3)		/* WID_ShapeEvent    */
#define WID_ROLLOVER  (SDL_USEREVENT+4)		/* WID_RolloverEvent */

/* HACK: The WID event structures must fit within sizeof(union SDL_Event) */

typedef struct WID_CreateEvent_s
{
	Uint8    type;
	rect_t   *rect;
	Uint32   flags;
} WID_CreateEvent;

typedef struct WID_DestroyEvent_s
{
	Uint8    type;
} WID_DestroyEvent;

typedef struct WID_CommandEvent_s
{
	Uint8    type;
	HWIDGET  sender;
	Uint32   id;
} WID_CommandEvent;

typedef struct WID_ShapeEvent_s
{
	Uint8    type;
	rect_t   *rect;
} WID_ShapeEvent;

typedef struct WID_RolloverEvent_s
{
	Uint8    type;
	int      leaving;		/* 0 = entering, non-0 = leaving */
} WID_RolloverEvent;

/* callback function types */
typedef int (*WIDGET_EVENT_CB)(HWIDGET, SDL_Event *, void *);
typedef void (*WIDGET_DRAW_CB)(HWIDGET, const rect_t *, void *);

/* widget class data */
typedef struct widcls_s 
{
	WIDGET_EVENT_CB   eventcb;
	WIDGET_DRAW_CB    drawcb;
} widcls_t;

/* widget flags */
#define WIDGET_VISIBLE  0x00000001
#define WIDGET_ACTIVE   0x00000002

/* default font */
extern struct font_s *deffont;

/* widget management */
extern int DefaultEventHandler(HWIDGET, SDL_Event *, void *);
extern HWIDGET GetParentWidget(HWIDGET);
extern HWIDGET GetFirstChildWidget(HWIDGET);
extern HWIDGET GetNextWidget(HWIDGET);
extern Uint32 GetWidgetID(HWIDGET);
extern void SetCaptureWidget(HWIDGET);
extern HWIDGET GetCaptureWidget(void);
extern void SetWidgetFocus(HWIDGET);
extern HWIDGET GetWidgetFocus(void);
extern HWIDGET CreateWidget(widcls_t *, int, int, int, int, HWIDGET, Uint32, Uint32);
extern void SetWidgetData(HWIDGET, void *);
extern void *GetWidgetData(HWIDGET);
extern void SetWidgetRect(HWIDGET, const rect_t *);
extern void GetWidgetRect(HWIDGET, rect_t *);
extern void ChangeWidgetFlags(HWIDGET, Uint32, Uint32);
extern Uint32 GetWidgetFlags(HWIDGET);
extern void DestroyWidget(HWIDGET);
extern void DestroyAllWidgets(void);
extern HWIDGET PointWidget(int, int);
extern int DispatchWidgetCommand(HWIDGET, Uint32);

/* widget execution */
extern void ProcessEvent(SDL_Event *);
extern void DrawWidgets(void);

/* guilib management */
extern void InitGuilib(struct font_s *);

/* button */
extern widcls_t widcls_button;
extern void SetButtonImages(HWIDGET, SDL_Surface *, SDL_Surface *, SDL_Surface *, SDL_Surface *);
extern void SetButtonLabel(HWIDGET, const char *);

/* picture (widget data is a pointer to an sdl surface) */
extern widcls_t widcls_picture;
