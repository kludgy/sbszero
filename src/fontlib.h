/*
 *  This is a rewritten version of SFONT by Darren Grant <darn@gameslate.com>
 *  SFONT - SDL Font Library by Karl Bartel <karlb@gmx.net>
 */

#define MAXFONTCHARS 512

typedef struct font_s
{
	SDL_Surface  *src;				/* bitmap typeface source          */
	int          x[MAXFONTCHARS];	/* src x -> left side of character */
	int          w[MAXFONTCHARS];	/* character widths                */
	int          h;					/* character height                */
} font_t;

#define JUSTIFY_LEFT    0
#define JUSTIFY_CENTER  1
#define JUSTIFY_RIGHT   2

extern font_t *CreateFont(SDL_Surface *);
extern void FreeFont(font_t *);
extern int TextWidth(font_t *, const char *);
extern void DrawText(font_t *, SDL_Surface *, int, int, int, const char *);
