enum
{
	CMD_UP = 0,
	CMD_DOWN,
	CMD_LEFT,
	CMD_RIGHT,
	CMD_FIRE,
	NUMCMDS
};

#define CMD_UP_B		(1 << CMD_UP)
#define CMD_DOWN_B		(1 << CMD_DOWN)
#define CMD_LEFT_B		(1 << CMD_LEFT)
#define CMD_RIGHT_B		(1 << CMD_RIGHT)
#define CMD_FIRE_B		(1 << CMD_FIRE)

#define CMD_MOVEV_MASK	(CMD_UP_B | CMD_DOWN_B)
#define CMD_MOVEH_MASK	(CMD_LEFT_B | CMD_RIGHT_B)
#define CMD_MOVE_MASK	(CMD_MOVEH_M | CMD_MOVEV_M)

enum
{
	CTL_KEYBOARD = 0,
	CTL_JOYSTICK,
	NUMCTLS
};

typedef struct ctlbind_s
{
	int   ctltype;				/* CTL_xxx */
	int   keybinds[NUMCMDS];	/* only applies to keyboard */

	struct
	{
		int     devicenum;
		int     buttonnum;
		Sint16  deadhorz[2];	/* deadspace ranges */
		Sint16  deadvert[2];
	} joy;

} ctlbind_t;

extern ctlbind_t ctlbinds[MAXPLAYERS];
extern Uint32 *TranslateBindings(void);
extern void InitControllers(void);
