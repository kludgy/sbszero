#include <stdlib.h>
#include <memory.h>
#include <limits.h>
#include <SDL.h>
#include "conlib.h"
#include "renlib.h"
#include "fontlib.h"
#include "guilib.h"

typedef struct widget_s
{
	rect_t            rect;
	Uint32			  flags;
	Uint32            id;
	void              *data;		/* user-defined */
	WIDGET_EVENT_CB   eventcb;
	WIDGET_DRAW_CB    drawcb;
	struct widget_s   *parent,
					  *firstchild, *lastchild,
					  *nextsib, *prevsib;
} widget_t;

/*
 * DefaultEventHandler()
 *
 * For general widget events.
 */
int DefaultEventHandler(HWIDGET widget, SDL_Event *event, void *data)
{
	return 0; /* event not handled */
}

font_t *deffont = 0;			/* default font */
static widget_t *capture = 0;	/* mouse button capture */
static widget_t *rollover = 0;	/* the widget that the mouse is over */
static widget_t *focus = 0;		/* focal widget (gets keyboard precedence) */
static widget_t root =			/* parent of all widgets; never destroyed */
{
	{ INT_MIN/2, INT_MIN/2, INT_MAX, INT_MAX },		/* rect				*/
	WIDGET_VISIBLE | WIDGET_ACTIVE,					/* flags			*/
	0,												/* id				*/
	0,												/* data				*/
	DefaultEventHandler,							/* eventcb			*/
	0,												/* drawcb			*/
	0,												/* parent			*/
	0, 0,											/* child first/last	*/
	0, 0											/* sib next/prev	*/
};

/*
 * GetParentWidget()
 *
 * Return the parent of the specified widget, or null if the widget is not a 
 * child.
 */
HWIDGET GetParentWidget(HWIDGET widget)
{
	/* don't allow access to the root widget */
	if (widget->parent == &root)
		return 0;

	return (HWIDGET)widget->parent;
}

/*
 * GetFirstChildWidget()
 *
 * Return the first child of the specified widget.
 */
HWIDGET GetFirstChildWidget(HWIDGET widget)
{
	return widget->firstchild;
}

/*
 * GetNextWidget()
 *
 * Return the next sibling.
 */
HWIDGET GetNextWidget(HWIDGET widget)
{
	return widget->nextsib;
}

/*
 * GetWidgetID()
 *
 * Return the resource ID for the specified widget.
 */
Uint32 GetWidgetID(HWIDGET widget)
{
	return widget->id;
}

/*
 * SetCaptureWidget()
 *
 * Specify a widget to capture all mouse button events, or null to resume
 * default behaviour.
 */
void SetCaptureWidget(HWIDGET widget)
{
	capture = widget;
}

/*
 * GetCaptureWidget()
 *
 * Returns the widget that's capturing all mouse button events.  May be
 * null.
 */
HWIDGET GetCaptureWidget(void)
{
	return capture;
}

/*
 * SetWidgetFocus()
 *
 * Specifies which widget should receive keyboard events first, or null if
 * order is not important.
 */
void SetWidgetFocus(HWIDGET widget)
{
	focus = widget;
}

/*
 * GetWidgetFocus()
 *
 * Returns the current focal widget (widget that gets keyboard events first).
 * May be null.
 */
HWIDGET GetWidgetFocus(void)
{
	return focus;
}

/*
 * CreateWidget()
 *
 * Allocate memory for and initialize a new widget structure.  If parent is
 * null, the new widget gets inserted beneath root.
 */
HWIDGET CreateWidget(widcls_t *cls, int x, int y, int w, int h, HWIDGET parent, Uint32 flags, Uint32 id)
{
	WID_CreateEvent  event;
	widget_t         *widget;

	static widcls_t defwidcls =
	{
		DefaultEventHandler,	/* eventcb */
		0						/* drawcb  */
	};

	widget = malloc(sizeof(widget_t));

	if (!widget)
		Error("CreateWidget: Out of memory\n");

	memset(widget, 0, sizeof(widget_t));

	/* copy class info */
	if (!cls)
		cls = &defwidcls;

	widget->eventcb = cls->eventcb;
	widget->drawcb = cls->drawcb;

	if (!widget->eventcb)
		widget->eventcb = DefaultEventHandler;

	/* set position and dimensions */
	widget->rect.x = x;
	widget->rect.y = y;
	widget->rect.w = w;
	widget->rect.h = h;

	/* set flags */
	widget->flags = flags;

	/* set identifier */
	widget->id = id;

	/* link into hierarchy */
	if (!parent)
		parent = &root;	/* link to root if they don't specify a parent */

	widget->parent = parent;
	widget->nextsib = parent->firstchild;

	if (parent->firstchild)
		parent->firstchild->prevsib = widget;

	parent->firstchild = widget;

	if (!parent->lastchild)
		parent->lastchild = widget;

	/* let the widget handle further creation */
	event.type = WID_CREATE;
	event.rect = &widget->rect;
	widget->eventcb(widget, (SDL_Event *)&event, widget->data);

	return (HWIDGET)widget;
}

/*
 * SetWidgetData()
 *
 * Set a pointer to user information for the widget.
 */
void SetWidgetData(HWIDGET widget, void *data)
{
	widget->data = data;
}

/*
 * GetWidgetData()
 *
 * Retrieve the information set by a previous call to SetWidgetData().
 */
void *GetWidgetData(HWIDGET widget)
{
	return widget->data;
}

/*
 * SetWidgetRect()
 *
 * Resize a widget.  A WID_SHAPE event is sent to the widget after its rect
 * is modified.
 */
void SetWidgetRect(HWIDGET widget, const rect_t *rect)
{
	WID_ShapeEvent  event;

	widget->rect = *rect;
	event.type = WID_SHAPE;
	event.rect = &widget->rect;
	widget->eventcb(widget, (SDL_Event *)&event, widget->data);
}

/*
 * GetWidgetRect()
 *
 * Copy shape info from the widget to the specified rect.
 */
void GetWidgetRect(HWIDGET widget, rect_t *rect)
{
	*rect = widget->rect;
}

/*
 * ChangeWidgetFlags()
 */
void ChangeWidgetFlags(HWIDGET widget, Uint32 add, Uint32 remove)
{
	widget->flags |= add;
	widget->flags &= ~remove;
}

/*
 * GetWidgetFlags()
 */
Uint32 GetWidgetFlags(HWIDGET widget)
{
	return widget->flags;
}

/*
 * DestroyWidget()
 *
 * Destroy and free widget's children, and then the widget itself.  The root
 * must never be destroyed.
 */
void DestroyWidget(HWIDGET widget)
{
	WID_DestroyEvent  event;
	widget_t          *child, *nextsib;

	/* children first */
	for (child = widget->firstchild; child; child = nextsib)
	{
		nextsib = child->nextsib;	/* store link before it's removed */
		DestroyWidget(child);
	}

	if (widget == &root)
		return;	/* root must never be destroyed */

	/* let the widget know it's about to be destroyed */
	event.type = WID_DESTROY;
	widget->eventcb(widget, (SDL_Event *)&event, widget->data);

	/* unlink it from the hierarchy */
	if (widget->nextsib)
		widget->nextsib->prevsib = widget->prevsib;
	else
		widget->parent->lastchild = widget->prevsib;

	if (widget->prevsib)
		widget->prevsib->nextsib = widget->nextsib;
	else
		widget->parent->firstchild = widget->nextsib;

	free(widget);
}

/*
 * DestroyAllWidgets()
 *
 * Destroys all widgets beneath root by invoking destruction of root.
 */
void DestroyAllWidgets(void)
{
	DestroyWidget((HWIDGET)&root);
}

/*
 * PointWidget_r()
 *
 * Recursively search for the topmost visible widget at (x,y).  Search starts
 * at the specified root.
 */
#define PTINRECT(sx, sy, rect) ( \
	(sx) >= (rect).x && (sx) < (rect).x + (rect).w && \
	(sy) >= (rect).y && (sy) < (rect).y + (rect).h \
)

static widget_t *PointWidget_r(int x, int y, widget_t *widget)
{
	widget_t *found, *child;

	if (!(widget->flags & WIDGET_VISIBLE) || !PTINRECT(x, y, widget->rect))
		return 0;

	/* top to bottom */
	for (child = widget->firstchild; child; child = child->nextsib)
	{
		found = PointWidget_r(x, y, child);

		if (found)
			return found;
	}

	/* parent last */
	return widget;
}

/*
 * PointWidget()
 *
 * Return the topmost visible widget under (x,y).
 */
HWIDGET PointWidget(int x, int y)
{
	HWIDGET found;

	found = PointWidget_r(x, y, &root);

	if (found == &root)
		found = 0;		/* don't expose root */

	return found;
}

/*
 * DispatchGenericEvent()
 *
 * Recursively send the event down through the tree, breaking out when an
 * eventcb call returns a non-zero result.  Inactive widget branches are
 * skipped.
 */
static int DispatchGenericEvent(SDL_Event *event, widget_t *widget)
{
	widget_t  *child;
	int       result;

	if (!(widget->flags & WIDGET_ACTIVE))
		return 0;

	/* parent first */
	result = widget->eventcb(widget, event, widget->data);

	if (result)
		return result;

	for (child = widget->firstchild; child; child = child->nextsib)
	{
		result = DispatchGenericEvent(event, child);

		if (result)
			return result;
	}

	return 0; /* no widget handled this event */
}

/*
 * DispatchWidgetCommand()
 *
 * Recursively invokes event callbacks for active widgets starting at root.
 * Breaks out if an event callback returns a non-zero result.  Returns 0
 * if no widgets handled the event.
 */
int DispatchWidgetCommand(HWIDGET sender, Uint32 id)
{
	WID_CommandEvent  event;

	event.type   = WID_COMMAND;
	event.sender = sender;
	event.id     = id;

	return DispatchGenericEvent((SDL_Event *)&event, &root);
}

/*
 * DispatchMouseMotionEvent()
 *
 * Send the event to the visible widget at (x,y).  If this widget is different
 * from the previous rollover widget, WID_ROLLOVER is sent to the old widget
 * and the new widget with 'leaving' values of 1 and 0, respectively.
 */
static int DispatchMouseMotionEvent(SDL_Event *event, widget_t *widget)
{
	widget_t           *found;
	WID_RolloverEvent  rollover_event;

	found = PointWidget_r(
		(int)((SDL_MouseMotionEvent *)event)->x, 
		(int)((SDL_MouseMotionEvent *)event)->y, 
		widget
	);

	if (found != rollover)
	{
		rollover_event.type = WID_ROLLOVER;

		if (rollover)
		{
			rollover_event.leaving = 1;
			rollover->eventcb(rollover, (SDL_Event *)&rollover_event, rollover->data);
		}

		if (found)
		{
			rollover_event.leaving = 0;
			found->eventcb(found, (SDL_Event *)&rollover_event, found->data);
		}

		rollover = found;
	}

	if (!found)
		return 0;

	return found->eventcb(found, event, found->data);
}

/*
 * DispatchMouseButtonEvent()
 *
 * Send the event to either the capture widget (if not null) or the visible
 * widget at (x,y).
 */
static int DispatchMouseButtonEvent(SDL_Event *event, widget_t *widget)
{
	widget_t  *found;

	if (capture)
		found = capture;
	else
		found = PointWidget_r(
			(int)((SDL_MouseButtonEvent *)event)->x, 
			(int)((SDL_MouseButtonEvent *)event)->y, 
			widget
		);

	if (!found)
		return 0;

	return found->eventcb(found, event, found->data);
}

/*
 * DispatchKeyboardEvent()
 *
 * Send the event to the focal widget (if not null), then to recursively to
 * each active widget in the tree, starting at the specified root.  The
 * function breaks out when an event callback returns a non-zero result.
 */
static int DispatchKeyboardEvent_r(SDL_Event *event, widget_t *widget)
{
	widget_t  *child;
	int       result;

	if (!(widget->flags & WIDGET_ACTIVE))
		return 0;

	if (widget != focus)
	{
		result = widget->eventcb(widget, event, widget->data);

		if (result)
			return result;
	}

	for (child = widget->firstchild; child; child = child->nextsib)
	{
		result = DispatchKeyboardEvent_r(event, child);

		if (result)
			return result;
	}

	return 0; /* no widget handled this keyboard event */
}

static int DispatchKeyboardEvent(SDL_Event *event, widget_t *widget)
{
	int  result;

	if (focus)
	{
		result = focus->eventcb(focus, event, focus->data);

		if (result)
			return result;
	}

	return DispatchKeyboardEvent_r(event, widget);
}

/*
 * DispatchCriticalEvent()
 *
 * Notify every widget of this event, regardless of settings.
 */
static int DispatchCriticalEvent(SDL_Event *event, widget_t *widget)
{
	widget_t  *child;

	widget->eventcb(widget, event, widget->data);

	for (child = widget->firstchild; child; child = child->nextsib)
		DispatchCriticalEvent(event, child);

	return 1; /* always report handled */
}

/*
 * ProcessEvent()
 *
 * Handle an SDL event, passing it off to widgets if applicable.
 */
#define EMASK_MOUSEM  (SDL_MOUSEMOTIONMASK)
#define EMASK_MOUSEB  (SDL_MOUSEBUTTONDOWNMASK | SDL_MOUSEBUTTONUPMASK)
#define EMASK_KEY     (SDL_KEYDOWNMASK | SDL_KEYUPMASK)
#define EMASK_GEN     (SDL_JOYEVENTMASK)
#define EMASK_SYS     (SDL_VIDEORESIZEMASK)

void ProcessEvent(SDL_Event *event)
{
	Uint32 emask;

	emask = SDL_EVENTMASK((Uint32)event->type);

	if (emask & EMASK_MOUSEM) DispatchMouseMotionEvent(event, &root);
	else if (emask & EMASK_MOUSEB) DispatchMouseButtonEvent(event, &root);
	else if (emask & EMASK_KEY) DispatchKeyboardEvent(event, &root);
	else if (emask & EMASK_GEN) DispatchGenericEvent(event, &root);
	else if (emask & EMASK_SYS) DispatchCriticalEvent(event, &root);

	/* some events are intentionally unhandled */
}

/*
 * DrawWidget()
 *
 * Recursively draw the widget and all of it's children.
 */
static SDL_Rect cliprect;

static void DrawWidget(widget_t *widget)
{
	widget_t *child;

	if (!(widget->flags & WIDGET_VISIBLE))
		return;

	if (widget->drawcb)
	{
		/* constrain drawing to this widget */
		cliprect.x = widget->rect.x;
		cliprect.y = widget->rect.y;
		cliprect.w = widget->rect.w;
		cliprect.h = widget->rect.h;

		SDL_SetClipRect(framebuf, &cliprect);
		widget->drawcb(widget, &widget->rect, widget->data);
	}

	/* draw in reverse order (bottom to top) */
	for (child = widget->lastchild; child; child = child->prevsib)
		DrawWidget(child);
}

/*
 * DrawWidgets()
 *
 * Handle the drawing of all widgets.  This function simply invokes 
 * DrawWidget() for the root.
 */
void DrawWidgets(void)
{
	DrawWidget(&root);
	SDL_SetClipRect(framebuf, NULL);
}

/*
 * InitGuilib()
 *
 * Call before any other widget functions to set up the system.
 */
void InitGuilib(font_t *font)
{
	if (!font)
		Error("InitGuilib: Null font\n");

	deffont = font;
	atexit(DestroyAllWidgets);
}
