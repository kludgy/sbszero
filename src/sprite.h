#define MAXSPRITES 5

typedef struct sprite_s
{
	SDL_Surface  *image;
	SDL_Rect     *rect;		/* subimage rect, or NULL to use entire image */
	int          x, y;
} sprite_t;

extern sprite_t sprites[MAXSPRITES];

extern void InitSprites(void);
extern void SetSpriteDims(int, int, int);
extern void DrawSprites(void);
extern void EraseSprites(void);
