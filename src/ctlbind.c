#include <stdlib.h>
#include <memory.h>
#include <SDL.h>
#include <SDL_Joystick.h>
#include "conlib.h"
#include "resource.h"
#include "ctlbind.h"

ctlbind_t ctlbinds[MAXPLAYERS] =
{
	{ CTL_KEYBOARD, { SDLK_w, SDLK_s, SDLK_a, SDLK_d, SDLK_LSHIFT }, { 0, 0, -1000, 1000, -1000, 1000 } },
	{ CTL_KEYBOARD, { SDLK_y, SDLK_h, SDLK_g, SDLK_j, SDLK_SPACE  }, { 0, 0, -1000, 1000, -1000, 1000 } },
	{ CTL_KEYBOARD, { SDLK_p, SDLK_SEMICOLON, SDLK_l, SDLK_QUOTE, SDLK_RCTRL }, { 0, 0, -1000, 1000, -1000, 1000 } },
	{ CTL_KEYBOARD, { SDLK_UP, SDLK_DOWN, SDLK_LEFT, SDLK_RIGHT, SDLK_END }, { 0, 0, -1000, 1000, -1000, 1000 } },
	{ CTL_KEYBOARD, { SDLK_KP8, SDLK_KP5, SDLK_KP4, SDLK_KP6, SDLK_KP0 }, { 0, 0, -1000, 1000, -1000, 1000 } }
};

static Uint8 *keys;

static Uint32 xlate_binding_keyboard(ctlbind_t *bind)
{
	int     i;
	Uint32  mask;

	mask = 0;

	for (i = 0; i < NUMCMDS; i++) 
	{
		if (keys[bind->keybinds[i]] == SDL_PRESSED) 
			mask |= 1 << i;
	}

	return mask;
}

#define MAXJOYS 16

static SDL_Joystick *joys[MAXJOYS];
static int numjoys;

static Uint32 xlate_binding_joystick(ctlbind_t *bind)
{
	Uint32        mask;
	Sint16        jpos;
	SDL_Joystick  *joy;

	mask = 0;
	joy = joys[bind->joy.devicenum];

	if (SDL_JoystickGetButton(joy, bind->joy.buttonnum))
		mask |= CMD_FIRE_B;

	jpos = SDL_JoystickGetAxis(joy, 0);

	     if (jpos < bind->joy.deadhorz[0]) mask |= CMD_LEFT_B;
	else if (jpos > bind->joy.deadhorz[1]) mask |= CMD_RIGHT_B;
	
	jpos = SDL_JoystickGetAxis(joy, 1);
	
	     if (jpos < bind->joy.deadvert[0]) mask |= CMD_UP_B;
	else if (jpos > bind->joy.deadvert[1]) mask |= CMD_DOWN_B;

	return mask;
}

/* Binding translators: generate a command mask based on data in the specified
 * ctlbind_s instance.
 */
typedef Uint32 (*XLATE_BINDING_CB)(ctlbind_t *);

static XLATE_BINDING_CB binding_xlators[NUMCTLS] =
{
	xlate_binding_keyboard,
	xlate_binding_joystick
};

static Uint32 xlate_results[MAXPLAYERS];

Uint32 *TranslateBindings(void)
{
	int        i;
	ctlbind_t  *bind;

	keys = SDL_GetKeyState(0);
	bind = ctlbinds;

	for (i = 0; i < MAXPLAYERS; i++, bind++)
		xlate_results[i] = binding_xlators[bind->ctltype](bind);

	return xlate_results;
}

static void FreeControllers(void)
{
	int  i;

	for (i = 0; i < numjoys; i++)
	{
		if (joys[i])
			SDL_JoystickClose(joys[i]);
	}
}

void InitControllers(void)
{
	int  i;

	numjoys = SDL_NumJoysticks();
	numjoys = min(numjoys, MAXJOYS);

	if (numjoys)
		memset(joys, 0, sizeof(SDL_Joystick *) * numjoys);

	for (i = 0; i < numjoys; i++)
	{
		joys[i] = SDL_JoystickOpen(i);

		if (!joys[i])
			Warning("InitControllers: device %i: %s\n", i, SDL_GetError());
	}

	atexit(FreeControllers);
}

