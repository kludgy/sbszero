#include <string.h>
#include "conlib.h"

/*
 * ExtractPath()
 *
 * Copy path from path+file string into the specified buffer.
 */
void ExtractPath(const char *file, char *buf, int bufsize)
{
	int i;

	if (!bufsize)
	{
		Warning("ExtractPath: 0 size buffer\n");
		return;
	}

	/* Determine length of file string to last slash.
	 * The last slash is included in this length.
	 */
	for (i = strlen(file)-1; i >= 0; i--)
		if (file[i] == '/' || file[i] == '\\')
			break;

	i += 2;	/* add one extra for null char */

	if (i > bufsize)
	{
		Warning("ExtractPath: Buffer overflow\n");
		i = bufsize;
	}

	strncpy(buf, file, i-1);
	buf[i-1] = 0;
}
