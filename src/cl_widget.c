#include <math.h>
#include <SDL.h>
#include "renlib.h"
#include "guilib.h"
#include "cl_gui.h"
#include "resource.h"
#include "sprite.h"

/****************************************************************************
  clientwidget creation
 ****************************************************************************/

static int ClientWidgetKeyHandler(SDL_KeyboardEvent *event)
{
	if (event->keysym.sym == SDLK_ESCAPE)
	{
		ShowScreen(SCREEN_FRONTEND);
		return 1;
	}

	return 0;
}

static int ClientWidgetCreate(HWIDGET widget, WID_CreateEvent *event)
{
	return 1;
}

static int ClientWidgetEventHandler(HWIDGET widget, SDL_Event *event, void *data)
{
	switch (event->type)
	{
		case SDL_KEYDOWN:
			return ClientWidgetKeyHandler((SDL_KeyboardEvent *)event);

		case WID_CREATE:
			return ClientWidgetCreate(widget, (WID_CreateEvent *)event);
	}

	return DefaultEventHandler(widget, event, data);
}

static void ClientWidgetDrawHandler(HWIDGET widget, const rect_t *rect, void *data)
{
	int i, x, n;

	static int framenum = 0;
	static int anim[5][4] = {
		{ PFRAME_WALK_RIGHT+0, PFRAME_WALK_RIGHT+1, PFRAME_WALK_RIGHT+0, PFRAME_WALK_RIGHT+2 },
		{ PFRAME_WALK_UP+0, PFRAME_WALK_UP+1, PFRAME_WALK_UP+0, PFRAME_WALK_UP+2 },
		{ PFRAME_WALK_LEFT+0, PFRAME_WALK_LEFT+1, PFRAME_WALK_LEFT+0, PFRAME_WALK_LEFT+2 },
		{ PFRAME_WALK_DOWN+0, PFRAME_WALK_DOWN+1, PFRAME_WALK_DOWN+0, PFRAME_WALK_DOWN+2 },
		{ PFRAME_DIE+0, PFRAME_DIE+1, PFRAME_DIE+0, PFRAME_DIE+2 },
	};

	n = (framenum/5) % 4;

	/* draw avatar pframes */
	for (i = 0, x = 64; i < MAXPLAYERS; i++, x += 64)
	{
		SetSpriteDims(i, pframe_rects[0].w, pframe_rects[0].h);
		sprites[i].image = avatars[i].pframes;
		sprites[i].rect = &pframe_rects[anim[i][n]];
		sprites[i].x = x;
		sprites[i].y = x;
	}

	framenum++;
}

HWIDGET CreateClientWidget(void)
{
	widcls_t  cls;

	/* the entire client is encapsulated in a fullscreen widget */
	cls.eventcb = ClientWidgetEventHandler;
	cls.drawcb = ClientWidgetDrawHandler;

	return CreateWidget(&cls, 0, 0, framebuf->w, framebuf->h, 0, 0, 0);
}
