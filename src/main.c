#include <stdlib.h>
#include <direct.h>
#include <SDL.h>
#include <SDL_image.h>
#include "conlib.h"
#include "filelib.h"
#include "renlib.h"
#include "resource.h"
#include "ctlbind.h"
#include "cl_gui.h"
#include "sprite.h"

/*
 * SetRootDir()
 *
 * Set the current directory to the executable directory.  All support files
 * are assumed to reside at/below this level.
 */
static void SetRootDir(const char *cmd)
{
	char rootdir[BUFSIZ];

	ExtractPath(cmd, rootdir, BUFSIZ);

	if (chdir(rootdir))
		Error("SetRootDir: Couldn't set directory: %s\n", rootdir);
}

/*
 * Shutdown()
 *
 * Exit handler for Startup().
 */
Uint32 starttm, framecount;

static void Shutdown(void)
{
	Uint32  elapsed;

	elapsed = SDL_GetTicks() - starttm;
	printf("%5.2f fps over %u frames\n", (float)1000 * framecount / elapsed, framecount);
	SDL_Quit();
}

/*
 * Startup()
 *
 * Initialize all program variables so that we can quit cleanly at any time
 * by invoking exit().
 */
static void Startup(void)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		Error("Couldn't initialize SDL: %s\n", SDL_GetError());

	PreinitResources();

	atexit(Shutdown);

	SDL_WM_SetCaption("Super Bomb Squad", "Super Bomb Squad");
	r_Init();
	InitSprites();
	LoadResources();
	InitGuilib(fonts[FONT_DEF]);
	InitControllers();
}

static void EventLoop(void)
{
	SDL_Event  event;

	starttm = SDL_GetTicks();
	framecount = 0;

	while (1)
	{
		DrawWidgets();
		DrawSprites();
		SDL_Flip(framebuf);

		while (SDL_PollEvent(&event)) 
		{
			switch (event.type) 
			{
				case SDL_QUIT:
					exit(0);
			}

			ProcessEvent(&event);
		}

		EraseSprites();
		SDL_Delay(1);			/* free time slice */
		framecount++;
	}
}

/*
 * main()
 */
int main(int argc, char *argv[])
{
	SetRootDir(argv[0]);
	Startup();
	CreateScreens();
	ShowScreen(SCREEN_FRONTEND);
	EventLoop();

	return 0;
}
