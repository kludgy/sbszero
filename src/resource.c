#include <stdlib.h>
#include <memory.h>
#include <SDL.h>
#include <SDL_image.h>
#include "renlib.h"
#include "conlib.h"
#include "fontlib.h"
#include "resource.h"

static SDL_Surface *IMG_LoadFormatted(const char *path, SDL_PixelFormat *vfmt, int soft)
{
	SDL_Surface  *src, *dst;

	src = IMG_Load(path);

	if (!src)
		return NULL;

	dst = SDL_ConvertSurface(src, vfmt, soft ? SDL_SWSURFACE : SDL_HWSURFACE);
	SDL_FreeSurface(src);

	return dst;
}

static SDL_Surface *IMG_LoadNative(const char *path)
{
	return IMG_LoadFormatted(path, framebuf->format, 0);
}

/*
 *  CacheIndex()
 *
 *  Copy a linefeed-seperated list of names in a text file to an array of
 *  given size.
 */
static int CacheIndex(const char *idxname, const char *defname, int maxnames, char *namebuf)
{
	char  iob[BUFSIZ];
	FILE  *fh;
	int   len, n;

	fh = fopen(idxname, "r");

	if (!fh)
	{
		/* use default name if the index doesn't exist */
		Warning("Couldn't open \"%s\" for input\n", idxname);
		goto exit;
	}

	n = 0;

	while (fgets(iob, BUFSIZ-1, fh))
	{
		len = strlen(iob);

		if (iob[len-1] == '\n')
			iob[len-1] = 0;

		if (iob[0] == 0)
			continue;

		if (n >= maxnames)
		{
			Warning("CacheIndex: maxnames (%i)\n", maxnames);
			return n;
		}

		strcpy(namebuf+(NAMESIZ*n), iob);
		n++;
	}

	fclose(fh);

exit:
	if (!n && maxnames >= 1)
	{
		/* guarantee that there is at least one entry */
		strncpy(namebuf[0], defname, NAMESIZ-1);
		n++;
	}

	return n;
}

/*
 *  LoadSaturatedImage()
 *
 *  Load the specified image and blend in colour for all grey pixels.
 */
static SDL_Surface *LoadSaturatedImage(const char *path, Uint32 colour)
{
	int             i;
	Uint8           r0, g0, b0;
	Uint8           r, g, b, v;
	Uint32          *pel;
	SDL_Surface     *src, *dst;
	SDL_PixelFormat fmt;
	
	memset(&fmt, 0, sizeof(SDL_PixelFormat));

	fmt.BitsPerPixel  = 32;
	fmt.BytesPerPixel = 4;
	fmt.Rmask         = 0xff000000;
	fmt.Gmask         = 0x00ff0000;
	fmt.Bmask         = 0x0000ff00;
	fmt.Amask         = 0x000000ff;
	fmt.Rshift        = 24;
	fmt.Gshift        = 16;
	fmt.Bshift        = 8;
	fmt.Ashift        = 0;

	src = IMG_LoadFormatted(path, &fmt, 1);		/* operate in software to avoid locking */

	if (!src)
	{
		Warning("LoadSaturatedImage: %s\n", IMG_GetError());
		return NULL;
	}

	pel = src->pixels;

	/* input colour in rgba format */
	r0 = (colour & 0xff000000) >> 24;
	g0 = (colour & 0x00ff0000) >> 16;
	b0 = (colour & 0x0000ff00) >> 8;

	/* replace unsaturated pixels with colour */
	for (i = (int)src->w * src->h; i; i--, pel++)
	{
		if ((*pel & 0xffffff00) == 0xff00ff00)
			continue;           /* skip transparent */

		v = (*pel & 0xff000000) >> 24;

		if (v != ((*pel & 0x00ff0000) >> 16) || 
			v != ((*pel & 0x0000ff00) >> 8))
			continue;           /* skip saturated */

		r = v * r0 / 255;
		g = v * g0 / 255;
		b = v * b0 / 255;

		*pel = (r << 24) | (g << 16) | (b << 8);
	}

	/* convert to native format */
	dst = SDL_ConvertSurface(src, framebuf->format, SDL_HWSURFACE);
	SDL_FreeSurface(src);

	if (!dst)
		Error("LoadSaturatedImage: %s\n", SDL_GetError());

	return dst;
}

SDL_Rect pframe_rects[NUMPFRAMES] =
{
	{ 0, 0, 36, 44 },		// PFRAME_WALK_DOWN
	{ 37, 0, 36, 44 },
	{ 74, 0, 36, 44 },
	{ 0, 45, 36, 44 },		// PFRAME_WALK_UP
	{ 37, 45, 36, 44 },
	{ 74, 45, 36, 44 },
	{ 0, 90, 36, 44 },		// PFRAME_WALK_LEFT
	{ 37, 90, 36, 44 },
	{ 74, 90, 36, 44 },
	{ 0, 135, 36, 44 },		// PFRAME_WALK_RIGHT
	{ 37, 135, 36, 44 },
	{ 74, 135, 36, 44 },
	{ 0, 180, 36, 44 },		// PFRAME_DIE
	{ 37, 180, 36, 44 },
	{ 74, 180, 36, 44 },
};

avatar_t  avatars[MAXPLAYERS];
char      avatarnames[MAXAVATARS][NAMESIZ];
int       numavatars;

static void FreeAvatarContents(avatar_t *avatar)
{
	if (avatar->pframes)
		SDL_FreeSurface(avatar->pframes);

	memset(avatar, 0, sizeof(avatar_t));
}

static Uint32 avatarcolours[MAXPLAYERS] =
{
	0xffffff00,     /* white */
	0xff602000,     /* red */
	0x0080ff00,     /* blue */
	0xffff0000,     /* yellow */
	0x80808000      /* black */
};

int LoadAvatarContents(const char *name, int playernum)
{
	avatar_t  *avatar;
	char      iob[BUFSIZ];
	Uint32    colorkey;

	avatar = avatars + playernum;

	FreeAvatarContents(avatar);
	_snprintf(iob, BUFSIZ-1, "avatar/%s/pframes.png", name);
	avatar->pframes = LoadSaturatedImage(iob, avatarcolours[playernum]);

	if (!avatar->pframes)
		return -1;

	colorkey = SDL_MapRGB(framebuf->format, 0xff, 0x00, 0xff);
	SDL_SetColorKey(avatar->pframes, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);

	return 0; /* ok */
}

theme_t theme;
char themenames[MAXTHEMES][NAMESIZ];
int numthemes;

SDL_Surface  *gfx[NUMGFX];
font_t       *fonts[NUMFONTS];

static const char *gfxpaths[NUMGFX] =
{
	"gfx/font1.png",
	"gfx/button_normal.png",
	"gfx/button_depress.png",
	"gfx/button_rollover.png",
	"gfx/button_inactive.png",
	"gfx/title.png",
	"gfx/ctl_header.png"
};

static int fontgfx[NUMFONTS] =
{
	GFX_FONT
};

static void FreeResources(void)
{
	int i;

	for (i = 0; i < NUMFONTS; i++)
		if (fonts[i]) FreeFont(fonts[i]);

	for (i = 0; i < NUMGFX; i++)
		if (gfx[i]) SDL_FreeSurface(gfx[i]);

	for (i = 0; i < MAXPLAYERS; i++)
		FreeAvatarContents(avatars + i);
}

void PreinitResources(void)
{
	memset(gfx, 0, sizeof(gfx));
	memset(fonts, 0, sizeof(fonts));
	memset(avatars, 0, sizeof(avatars));
}

void LoadResources(void)
{
	int          i;
	SDL_Surface  *convert;

	atexit(FreeResources);

	/*
	 * gfx
	 */
	for (i = 0; i < NUMGFX; i++)
	{
		gfx[i] = IMG_LoadNative(gfxpaths[i]);

		if (!gfx[i])
			Error("LoadResources: %s\n", IMG_GetError());
	}

	/*
	 * fonts
	 */
	for (i = 0; i < NUMFONTS; i++)
		fonts[i] = CreateFont(gfx[i]);

	/*
	 * custom resources
	 */
	numavatars = CacheIndex("avatar/index.dir", "Classic", MAXAVATARS, (char *)avatarnames);
	numthemes  = CacheIndex( "theme/index.dir", "Classic",  MAXTHEMES, (char *) themenames);

	/* load defaults */
	for (i = 0; i < MAXPLAYERS; i++)
		LoadAvatarContents(avatarnames[0], i);

	//LoadThemeContents(themenames[0]);
}

enum
{
	TILE_PATH = 0,
	TILE_SOFT,
	TILE_HARD,
	NUMTILES
};

SDL_Rect tile_rects[NUMTILES] =
{
	{ 0, 0, 32, 32 },		// TILE_PATH
	{ 32, 0, 32, 32 },		// TILE_SOFT
	{ 64, 0, 32, 32 },		// TILE_HARD
};

typedef struct tile_s
{
	int     image;
	int     isotope;
	Uint32  flags;
} tile_t;

#define CEL_SOLID      1    /* impassable */
#define CEL_PERMA      2    /* cannot remove solid flag */
#define CEL_KILLER     4    /* kills players instantly */
#define CEL_OPAQUE     8    /* cast shadows */

tile_t tiles[NUMTILES] =
{
	{ TILE_PATH, TILE_PATH, 0 },
	{ TILE_SOFT, TILE_PATH, CEL_SOLID | CEL_OPAQUE },
	{ TILE_HARD, TILE_PATH, CEL_SOLID | CEL_OPAQUE | CEL_PERMA },
};
