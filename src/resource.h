#define ID_USER     32768    /* procedural command IDs start here */
#define MAXPLAYERS  5
#define NAMESIZ     32
#define MAXAVATARS  16
#define MAXTHEMES   16

enum
{
	GFX_FONT = 0,
	GFX_BUTTON_0,
	GFX_BUTTON_1,
	GFX_BUTTON_2,
	GFX_BUTTON_3,
	GFX_TITLE,
	GFX_CTLHEADER,
	NUMGFX
};

enum
{
	FONT_DEF = 0,
	NUMFONTS
};

enum
{
	PFRAME_WALK_DOWN = 0,
	PFRAME_WALK_UP = 3,
	PFRAME_WALK_LEFT = 6,
	PFRAME_WALK_RIGHT = 9,
	PFRAME_DIE = 12,
	NUMPFRAMES = 15
};

typedef struct avatar_s
{
	SDL_Surface  *pframes;
} avatar_t;

typedef struct theme_s
{
	SDL_Surface  *image;
} theme_t;

extern avatar_t       avatars[MAXPLAYERS];
extern char           avatarnames[MAXAVATARS][NAMESIZ];
extern int            numavatars;
extern SDL_Rect       pframe_rects[NUMPFRAMES];
extern SDL_Surface    *gfx[NUMGFX];
extern struct font_s  *fonts[NUMFONTS];

extern int LoadAvatarContents(const char *, int playernum);
extern void PreinitResources(void);
extern void LoadResources(void);
