#define MAP_W          21   /* number of cels across and down */
#define MAP_H          15
#define MAP_OFS_X     -16   /* position of top left corner in screen */
#define MAP_OFS_Y      16

#define TILE_W         32
#define TILE_H         32

typedef struct cel_s
{
	int      images[2];
	int      numimages;
	int      tile;
	int      occupant;
	int      prize;
	Uint32   flags;
} cel_t;

extern cel_t map[MAP_W][MAP_H];

extern void SetFramework(void);
extern void DistributePrizes(void);
extern void DrawCel(int x, int y);
extern void DestroyTile(int x, int y);
