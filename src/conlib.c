#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

/*
 * Error()
 *
 * Print to stderr and halt the program.
 */
void Error(char *text, ...)
{
	va_list marker;
	va_start(marker, text);
	vfprintf(stderr, text, marker);
	exit(-1);
}

/*
 * Warning()
 *
 * Print to stderr.
 */
void Warning(char *text, ...)
{
	va_list marker;
	va_start(marker, text);
	vfprintf(stderr, text, marker);
}
