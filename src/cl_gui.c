#include <SDL.h>
#include "guilib.h"
#include "cl_gui.h"

extern HWIDGET CreateFrontend(void);
extern HWIDGET CreateClientWidget(void);

static HWIDGET (*createfns[NUMSCREENS])(void) =
{
	CreateFrontend,
	CreateClientWidget
};

static HWIDGET screens[NUMSCREENS];
static screen_t curscreen;

void CreateScreens(void)
{
	int i;

	for (i = 0; i < NUMSCREENS; i++)
		screens[i] = (*createfns[i])();

	curscreen = -1;
}

#define WIDGET_ON (WIDGET_VISIBLE | WIDGET_ACTIVE)

void ShowScreen(screen_t screen)
{
	/* hide the old and show the new */

	if (curscreen != -1)
		ChangeWidgetFlags(screens[curscreen], 0, WIDGET_ON);

	ChangeWidgetFlags(screens[screen], WIDGET_ON, 0);

	curscreen = screen;
}
