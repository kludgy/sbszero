#include <stdlib.h>
#include <memory.h>
#include <SDL.h>
#include "conlib.h"
#include "renlib.h"
#include "guilib.h"
#include "fontlib.h"
#include "resource.h"
#include "ctlbind.h"
#include "cl_gui.h"

#define ID_QUIT			1
#define ID_BACK         2
#define ID_STARTLOCAL	10
#define ID_STARTNET		11
#define ID_JOIN         12
#define ID_CTLSETUP		13

/* the various frontend menus */
enum
{
	GUI_MAINMENU = 0,
	GUI_CTLSETUP,
	NUMGUIS
};

static rect_t  title, menu;	/* calculated when frontend is created or resized */
static menustate = GUI_MAINMENU;
static HWIDGET widgets[NUMGUIS];

/*
 * ShowMenu()
 *
 * Parameter is one of the GUI_xxx enums.  Shows the specified menu and hides 
 * the previous.
 */
static void ShowMenu(int menu)
{
	ChangeWidgetFlags(widgets[menustate], 0, WIDGET_ACTIVE | WIDGET_VISIBLE);
	ChangeWidgetFlags(widgets[menu], WIDGET_ACTIVE | WIDGET_VISIBLE, 0);
	menustate = menu;
}

/*
 * CenterRect()
 *
 * Generates rect data such that *free is centered in *frame.  'free' and 
 * 'centered' may both point to the same rect_t instance.
 */
static void CenterRect(const rect_t *free, const rect_t *frame, rect_t *centered)
{
	centered->w = free->w;
	centered->h = free->h;
	centered->x = (frame->w / 2 + frame->x) - (free->w / 2);
	centered->y = (frame->h / 2 + frame->y) - (free->h / 2);
}

/****************************************************************************
  main menu
 ****************************************************************************/

#define NUMBTNS 5

static int MainMenuCommandHandler(WID_CommandEvent *event)
{
	switch (event->id)
	{
		case ID_BACK: 
			exit(0);
	}

	return 0;					/* unhandled */
}

static int MainMenuCreate(HWIDGET widget, WID_CreateEvent *event)
{
	int      i;
	int      bnw, bnh;
	int      rowh, x, y;
	HWIDGET  button;
	Uint32   flags;

	const char *bnlabels[NUMBTNS] = 
	{
		"Start Local Game",
		"Start Net Game",
		"Join Game",
		"Controller Setup",
		"Quit"
	};

	Uint32 bnids[NUMBTNS] =
	{
		ID_STARTLOCAL,
		0, /*ID_STARTNET,*/
		0, /*ID_JOINNET,*/
		ID_CTLSETUP,
		ID_QUIT
	};

	/* create main menu buttons */
	bnw = event->rect->w - 16;
	bnh = 24;
	rowh = bnh + 4;
	x = event->rect->x + 8;
	y = event->rect->y + 8;

	for (i = 0; i < NUMBTNS-1; i++, y += rowh)
	{
		flags = WIDGET_VISIBLE;

		if (bnids[i])
			flags |= WIDGET_ACTIVE;

		button = CreateWidget(&widcls_button, x, y, bnw, bnh, widget, flags, bnids[i]);
		SetButtonImages(button, gfx[GFX_BUTTON_0], gfx[GFX_BUTTON_1], gfx[GFX_BUTTON_2], gfx[GFX_BUTTON_3]);
		SetButtonLabel(button, bnlabels[i]);
	}

	/* quit button is seperate */
	y = event->rect->y + event->rect->h - bnh - 8;
	button = CreateWidget(&widcls_button, x, y, bnw, bnh, widget, WIDGET_ACTIVE | WIDGET_VISIBLE, bnids[i]);
	SetButtonImages(button, gfx[GFX_BUTTON_0], gfx[GFX_BUTTON_1], gfx[GFX_BUTTON_2], gfx[GFX_BUTTON_3]);
	SetButtonLabel(button, bnlabels[i]);

	return 1;
}

static int MainMenuEventHandler(HWIDGET widget, SDL_Event *event, void *data)
{
	switch (event->type)
	{
		case WID_COMMAND:
			return MainMenuCommandHandler((WID_CommandEvent *)event);

		case WID_CREATE:
			return MainMenuCreate(widget, (WID_CreateEvent *)event);
	}

	return DefaultEventHandler(widget, event, data);
}

static void MainMenuDrawHandler(HWIDGET widget, const rect_t *rect, void *data)
{
	/* intentionally blank */
}

static HWIDGET CreateMainMenu(HWIDGET parent)
{
	HWIDGET   widget;
	widcls_t  menucls;
	rect_t    rect = { 0, 0, 320, 200 };

	menucls.eventcb = MainMenuEventHandler;
	menucls.drawcb = MainMenuDrawHandler;

	CenterRect(&rect, &menu, &rect);

	widget = CreateWidget(&menucls, rect.x, rect.y, rect.w, rect.h, parent, 0, 0);

	return widget;
}

/****************************************************************************
  controller setup menu
 ****************************************************************************/

typedef struct
{
	HWIDGET  bindbns[MAXPLAYERS][NUMCMDS];
	HWIDGET  typebns[MAXPLAYERS];
	Uint32   focusid;						/* button id for binding expecting input */
} ctlsetup_t;

#define CTLSETUP_ID_TOGGLE				ID_USER
#define CTLSETUP_ID_BIND(player, cmd) 	(ID_USER+MAXPLAYERS+(player)*NUMCMDS+(cmd))

static void GetBindSrc(int id, int *player, int *cmd)
{
	int i;

	i = id - ID_USER - MAXPLAYERS;

	*player = i / NUMCMDS;
	*cmd = i % NUMCMDS;
}

static void CtlSetup_KillBindFocus(ctlsetup_t *ldata)
{
	int playernum, cmdnum;

	GetBindSrc(ldata->focusid, &playernum, &cmdnum);
	SetButtonLabel(ldata->bindbns[playernum][cmdnum], SDL_GetKeyName(ctlbinds[playernum].keybinds[cmdnum]));
	SetWidgetFocus(0);
	ldata->focusid = 0;
}

static const char *ctlnames[NUMCTLS] = 
{
	"Keyboard",
	"Joystick"
};

static int CtlSetupKeyHandler(SDL_KeyboardEvent *event, ctlsetup_t *ldata)
{
	int  i, j;
	int  newkey;
	int  playernum, cmdnum;

	if (!ldata->focusid)
		return 0;			/* no binding button needing input */

	GetBindSrc(ldata->focusid, &playernum, &cmdnum);
	newkey = event->keysym.sym;

	/* find duplicate */
	for (i = 0; i < MAXPLAYERS; i++)
		for (j = 0; j < NUMCMDS; j++)
			if (newkey == ctlbinds[i].keybinds[j])
			{
				/* swap keys to prevent conflict */
				ctlbinds[i].keybinds[j] = ctlbinds[playernum].keybinds[cmdnum];
				SetButtonLabel(ldata->bindbns[i][j], SDL_GetKeyName(ctlbinds[i].keybinds[j]));
				goto done;
			}
done:

	/* update binding and button */
	ctlbinds[playernum].keybinds[cmdnum] = newkey;
	CtlSetup_KillBindFocus(ldata);

	return 1;
}

static int CtlSetupCommandHandler(WID_CommandEvent *event, ctlsetup_t *ldata)
{
	int      i, j;

	switch (event->id)
	{
		case ID_BACK: 
			if (ldata->focusid) 
				CtlSetup_KillBindFocus(ldata);

			ShowMenu(GUI_MAINMENU);
			return 1;
	}

	if (event->id >= CTLSETUP_ID_BIND(0, 0))
	{
		HWIDGET  focus;

		if (ldata->focusid)
		{
			/* abort previous query */
			GetBindSrc(ldata->focusid, &i, &j);
			focus = ldata->bindbns[i][j];
			SetButtonLabel(focus, SDL_GetKeyName(ctlbinds[i].keybinds[j]));

			if (ldata->focusid == event->id)
				SetWidgetFocus(0);
		}

		if (ldata->focusid != event->id)
		{
			/* query using the new bind button */
			GetBindSrc(event->id, &i, &j);
			focus = ldata->bindbns[i][j];
			SetButtonLabel(focus, "???");
			SetWidgetFocus(GetParentWidget(focus));
			ldata->focusid = event->id;
		}

		return 1;
	}
	else if (event->id >= CTLSETUP_ID_TOGGLE)
	{
		i = event->id - CTLSETUP_ID_TOGGLE;

		ctlbinds[i].ctltype++;

		if (ctlbinds[i].ctltype >= NUMCTLS)
			ctlbinds[i].ctltype = 0;

		SetButtonLabel(ldata->typebns[i], ctlnames[ctlbinds[i].ctltype]);

		return 1;
	}

	return 0;					/* unhandled */
}

static int CtlSetupCreate(HWIDGET widget, WID_CreateEvent *event)
{
	ctlsetup_t  *ldata;
	HWIDGET     newwidget;
	int         i, j, x0, x, y;

	ldata = malloc(sizeof(ctlsetup_t));

	if (!ldata)
		Error("CtlSetupCreate: Out of memory\n");

	SetWidgetData(widget, ldata);
	memset(ldata, 0, sizeof(ctlsetup_t));

	/* create the array of buttons corresponding to individual control bindings */
	x0 = event->rect->x + event->rect->w - 64*NUMCMDS;
	y = event->rect->y + 56;

	for (i = 0; i < MAXPLAYERS; i++, y += 28)
	{
		for (j = 0, x = x0; j < NUMCMDS; j++, x += 64)
		{
			newwidget = CreateWidget(
				&widcls_button, x, y, 60, 24, widget, 
				WIDGET_VISIBLE | WIDGET_ACTIVE, 
				CTLSETUP_ID_BIND(i, j)
			);

			SetButtonImages(newwidget, gfx[GFX_BUTTON_0], gfx[GFX_BUTTON_1], gfx[GFX_BUTTON_2], gfx[GFX_BUTTON_3]);
			SetButtonLabel(newwidget, SDL_GetKeyName(ctlbinds[i].keybinds[j]));
			ldata->bindbns[i][j] = newwidget;
		}

		/* create the key/joy button for this row */
		newwidget = CreateWidget(
			&widcls_button, x0 - 148, y, 128, 24, widget, 
			WIDGET_VISIBLE /*| WIDGET_ACTIVE*/, 
			CTLSETUP_ID_TOGGLE+i
		);

		SetButtonImages(newwidget, gfx[GFX_BUTTON_0], gfx[GFX_BUTTON_1], gfx[GFX_BUTTON_2], gfx[GFX_BUTTON_3]);
		SetButtonLabel(newwidget, ctlnames[ctlbinds[i].ctltype]);
		ldata->typebns[i] = newwidget;
	}

	/* create a back button */
	newwidget = CreateWidget(
		&widcls_button,
		event->rect->w / 2 + event->rect->x - 260 / 2,
		event->rect->y + event->rect->h - 32,
		260,
		24,
		widget,
		WIDGET_VISIBLE | WIDGET_ACTIVE,
		ID_BACK
	);

	SetButtonImages(newwidget, gfx[GFX_BUTTON_0], gfx[GFX_BUTTON_1], gfx[GFX_BUTTON_2], gfx[GFX_BUTTON_3]);
	SetButtonLabel(newwidget, "Done");

	return 1;
}

static void CtlSetupDestroy(ctlsetup_t *ldata)
{
	if (ldata)
		free(ldata);
}

static int CtlSetupEventHandler(HWIDGET widget, SDL_Event *event, void *data)
{
	switch (event->type)
	{
		case SDL_KEYDOWN:
			return CtlSetupKeyHandler((SDL_KeyboardEvent *)event, (ctlsetup_t *)data);

		case WID_COMMAND:
			return CtlSetupCommandHandler((WID_CommandEvent *)event, (ctlsetup_t *)data);

		case WID_CREATE:
			return CtlSetupCreate(widget, (WID_CreateEvent *)event);

		case WID_DESTROY:
			CtlSetupDestroy((ctlsetup_t *)data);
			return 1;
	}

	return DefaultEventHandler(widget, event, data);
}

static void CtlSetupDrawHandler(HWIDGET widget, const rect_t *rect, void *data)
{
	int       i, y;
	SDL_Rect  dstrect;

	static const char *rowlabels[MAXPLAYERS] =
	{
		"Controller 1",
		"Controller 2",
		"Controller 3",
		"Controller 4",
		"Controller 5"
	};

	/* draw the ctl header */
	dstrect.w = gfx[GFX_CTLHEADER]->w;
	dstrect.h = gfx[GFX_CTLHEADER]->h;
	dstrect.x = (Uint16)(rect->x + rect->w) - dstrect.w;
	dstrect.y = rect->y;

	SDL_BlitSurface(gfx[GFX_CTLHEADER], 0, framebuf, &dstrect);

	/* draw the row labels */
	y = rect->y + 60;

	for (i = 0; i < MAXPLAYERS; i++, y += 28)
		DrawText(deffont, framebuf, JUSTIFY_LEFT, rect->x, y, rowlabels[i]);
}

static HWIDGET CreateCtlSetup(HWIDGET parent)
{
	HWIDGET   widget;
	widcls_t  cls;
	rect_t    rect = { 0, 0, 600, 260 };

	cls.eventcb = CtlSetupEventHandler;
	cls.drawcb = CtlSetupDrawHandler;

	CenterRect(&rect, &menu, &rect);

	widget = CreateWidget(&cls, rect.x, rect.y, rect.w, rect.h, parent, 0, 0);
	
	return widget;
}

/****************************************************************************
  splash gfx (title)
 ****************************************************************************/

static void CreateSplashGfx(HWIDGET parent)
{
	HWIDGET  splash;
	rect_t   rect = { 0, 0, gfx[GFX_TITLE]->w, gfx[GFX_TITLE]->h };

	CenterRect(&rect, &title, &rect);
	splash = CreateWidget(&widcls_picture, rect.x, rect.y, rect.w, rect.h, parent, WIDGET_VISIBLE, 0);
	SetWidgetData(splash, gfx[GFX_TITLE]);
}

/****************************************************************************
  frontend creation
 ****************************************************************************/

static int FrontendCommandHandler(WID_CommandEvent *event)
{
	switch (event->id)
	{
		case ID_QUIT: 
			exit(0);

		case ID_STARTLOCAL:
			ShowScreen(SCREEN_CLIENT);
			return 1;

		case ID_CTLSETUP:
			ShowMenu(GUI_CTLSETUP);
			return 1;
	}

	return 0;					/* unhandled */
}

static int FrontendCreate(HWIDGET widget, WID_CreateEvent *event)
{
	/* partition title/menu space */
	title.x = title.y = 0;
	title.w = framebuf->w;
	title.h = gfx[GFX_TITLE]->h + 32;

	menu.x = 0;
	menu.y = title.h;
	menu.w = framebuf->w;
	menu.h = framebuf->h - menu.y;

	/* create the widgets */
	CreateSplashGfx(widget);
	widgets[GUI_MAINMENU] = CreateMainMenu(widget);
	widgets[GUI_CTLSETUP] = CreateCtlSetup(widget);

	return 1;
}

static int FrontendEventHandler(HWIDGET widget, SDL_Event *event, void *data)
{
	switch (event->type)
	{
		case SDL_KEYDOWN:
			if (((SDL_KeyboardEvent *)event)->keysym.sym == SDLK_ESCAPE)
				DispatchWidgetCommand(widget, ID_BACK);
			return 1;

		case WID_COMMAND:
			return FrontendCommandHandler((WID_CommandEvent *)event);

		case WID_CREATE:
			return FrontendCreate(widget, (WID_CreateEvent *)event);
	}

	return DefaultEventHandler(widget, event, data);
}

static void FrontendDrawHandler(HWIDGET widget, const rect_t *rect, void *data)
{
	/* just wipe the entire screen for now */
	SDL_FillRect(framebuf, 0, 0);
}

HWIDGET CreateFrontend(void)
{
	widcls_t  cls;
	HWIDGET   widget;

	/* the entire frontend is encapsulated in a fullscreen widget */
	cls.eventcb = FrontendEventHandler;
	cls.drawcb = FrontendDrawHandler;

	widget = CreateWidget(&cls, 0, 0, framebuf->w, framebuf->h, 0, 0, 0);
	ShowMenu(GUI_MAINMENU);

	return widget;
}
